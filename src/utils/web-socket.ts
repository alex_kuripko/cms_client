export function createWebSocketConnection(url: string): Promise<WebSocket> {
  const socket = new WebSocket(url);

  return new Promise((resolve, reject) => {
    socket.onopen = () => {
      resolve(socket);
    };

    socket.onerror = (error) => {
      reject(error);
    };
  });
}
